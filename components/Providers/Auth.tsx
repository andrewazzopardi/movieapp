import axios from 'axios';
import { useRouter } from 'next/router';
import React, { useEffect, useMemo, useState } from 'react'
import { AuthContext } from '../../hooks/context';
import { getGuestSessionApi, getGuestSessionRatedMoviesApi } from '../../utils/api';

type Props = {
  children: React.ReactNode;
}

function AuthProvider({ children }: Props) {
  const [user, setUser] = useState(null);

  const populateUser = async (nextUser) => {
    const populatedUser = { ...nextUser };
    const ratingsRequest = await axios.get(getGuestSessionRatedMoviesApi(populatedUser.guest_session_id));
    populatedUser.movieRatings = ratingsRequest.data.results;
    return populatedUser
  }

  const updateUser = async (nextUser) => {
    const populatedUser = await populateUser(nextUser);
    localStorage.setItem('user', JSON.stringify(populatedUser));
    setUser(populatedUser);
  }

  const authenticate = async () => {
    const authResponse = await axios.get(getGuestSessionApi());
    const nextUser = authResponse.data;
    console.log(nextUser);
    await updateUser(nextUser);
  }

  useEffect(() => {
    const userString = localStorage.getItem('user');
    if (!userString) {
      authenticate();
    } else {
      setUser(JSON.parse(userString));
    }
  }, []);
  
  const state = useMemo(() => ({
    user, updateUser,
  }), [user, setUser])
  
  return (
    <AuthContext.Provider value={state}>
      {children}
    </AuthContext.Provider>
  )
}

export default AuthProvider