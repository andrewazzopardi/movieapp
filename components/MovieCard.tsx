// card component to display movie details
import Image from 'next/image';
import Link from 'next/link';
import React from 'react'
import styled from 'styled-components';
import { movieApiImageBaseUrl } from '../utils/api';

const Description = styled.p`
  margin: 8px 0;
  font-size: 14px;
  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
  overflow: hidden;
`;

const InfoContainer = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  height: 0;
  padding: 0 8px;
  background: #ffffff20;
  border: 1px solid rgba(255, 255, 255, 0.39);
  border-radius: 16px 16px 0 0;
  color: white;
  text-shadow: 1px -1px 3px #000, -1px 1px 3px #000;
  box-shadow: 0 4px 30px rgba(0, 0, 0, 0.1);
  backdrop-filter: blur(8px);
  overflow: hidden;

  transition:
    height 0.2s ease-in-out,
    padding 0.2s ease-in-out;

  &:hover {
    height: 100%!important;
    border-radius: 4px;

    & > ${Description} {
      -webkit-line-clamp: 10;
    }
  }
`;

const Container = styled.div`
  overflow: hidden;
  border-radius: 4px;
  position: relative;
  aspect-ratio: 2/3;

  &:hover > ${InfoContainer} {
    height: 100px;
    padding: 8px;
  }
`;

const MoviePoster = styled(Image)`
  position: absolute;
  width: 100%;
  height: 100%;
  border-radius: 4px;
  object-fit: cover;
`;

const Title = styled.h1`
  margin: 0;
  font-size: 18px;
`;

type Props = {
  id: number;
  title: string;
  overview: string;
  poster_path: string;
}

function MovieCard({ id, title, overview, poster_path }: Props) {
  return (
    <Link href={`/movie/${id}`}>
      <Container>
        <MoviePoster src={movieApiImageBaseUrl+poster_path} layout="fill" />
        <InfoContainer>
          <Title>{title}</Title>
          <Description>{overview}</Description>
        </InfoContainer>
      </Container>
    </Link>
  )
}

export default MovieCard