import React from 'react'
import NextLink from 'next/link'
import styled from 'styled-components';

const StyledA = styled.a`
  transition: color 0.2s ease-in-out;
`;


type Props = {
  children: React.ReactNode;
  href: string;
}

function Link({ children, href }: Props) {
  return (
    <NextLink href={href}>
      <StyledA>{children}</StyledA>
    </NextLink>
  )
}

export default Link