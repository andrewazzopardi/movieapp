import styled from "styled-components";

const CardContainer = styled.div`
  display: grid;
  grid-gap: 24px;
  padding: 24px;
  grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
`;

export default CardContainer