import React from 'react'
import styled from 'styled-components';

const Container = styled.div`
  background: ${props => props.theme.colors.primary};
  padding: 8px;
  border-radius: 4px;
`;

type Props = {
  children: React.ReactNode;
}

function Card({ children}: Props) {
  return (
    <Container>
      {children}
    </Container>
  )
}

export default Card