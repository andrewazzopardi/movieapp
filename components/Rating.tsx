import React from 'react'
import styled from 'styled-components';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFilm } from '@fortawesome/free-solid-svg-icons'
import axios from 'axios';
import { getRateMovieApi } from '../utils/api';
import { useAuthContext } from '../hooks/context';
import { toast } from 'react-toastify';

const Wrapper = styled.div`
  color: white;
  text-align: center;
  filter: drop-shadow(0 0 2px #000000);
`;

const Container = styled.div`
  display: grid;
  grid-template-columns: repeat(5, auto);
  justify-content: center;
  color: white;
  font-size: 24px;
  grid-gap: 4px;

  &:hover {
    color: red;
  }
`;

const ChangeColorOnHoverSiblings = styled.span`
  cursor: pointer;
  transition: color 0.2s ease-in-out;

  &:hover ~ & {
    color: white;
  }
`;

type Props = {
  movieId: string;
}

function Rating({ movieId }: Props) {
  const { user, updateUser } = useAuthContext();
  
  const rateMovie = async (rating: number) => {
    try {
      await axios.post(getRateMovieApi(user.guest_session_id, movieId), { value: rating });
      updateUser(user);
      toast.success('Movie rated successfully');
    } catch (e) {
      toast.error('Error rating movie');
    }
  }
  
  return (
    <Wrapper>
      Rating
      <Container>
        {Array.from({ length: 5 }, (_, i) => (
          <ChangeColorOnHoverSiblings key={i}>
            <FontAwesomeIcon 
              icon={faFilm}
              onClick={() => rateMovie((i + 1) * 2)}
              color={(user?.movieRatings
                .filter(movie => movie.id == movieId)[0]
                ?.rating || 0) >= (i + 1) * 2 ? 'red' : undefined}
            />
          </ChangeColorOnHoverSiblings>
        ))}
      </Container>
    </Wrapper>
  )
}

export default Rating