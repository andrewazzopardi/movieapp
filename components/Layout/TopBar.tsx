import { useRouter } from 'next/router';
import React, { useState } from 'react'
import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFilm } from '@fortawesome/free-solid-svg-icons'
import { useLayoutContext } from '../../hooks/context';

const Container = styled.div`
  min-height: 64px;
  background: #d00000;
  color: ${props => props.theme.topBar.text};
  display: flex;
  align-items: center;
  padding: 8px 24px;
  justify-content: space-between;
  flex-wrap: wrap;
  z-index: 2;
  position: sticky;
  top: 0;

  @media (min-width: 480px) {
    background: ${props => props.theme.topBar.background};
  }
`;

const MenuIcon = styled.div`
  cursor: pointer;

  @media (min-width: 480px) {
    display: none;
  }
`;

const Title = styled.h1`
  margin: 8px auto 4px 16px;
  font-size: 24px;
  white-space: nowrap;
  width: 195px;

  @media (min-width: 480px) {
    margin: 0 auto 0 0;
  }
`;

const SearchForm = styled.form`
  margin: 4px 0 8px auto;
  display: flex;
  align-items: center;

  @media (min-width: 480px) {
    margin: 0;
  }
`;

const SearchInput = styled.input`
  border: none;
  border-radius: 4px 0 0 4px;
  padding: 8px;

  &:focus {
    outline: none;
  }
`;

const SearchButton = styled.button`
  border: none;
  border-radius: 0 4px 4px 0;
  padding: 8px;
  background: ${props => props.theme.colors.secondary};
  color: ${props => props.theme.colors.textSecondary};
  cursor: pointer;

  &:focus, &:hover {
    color: ${props => props.theme.colors.primary};
    outline: none;
    scale: 1.1;
  }

  &:active {
    color: ${props => props.theme.colors.textSecondary};
    scale: 1;
  }
`;

function TopBar() {
  const router = useRouter();
  const [searchText, setSearchText] = useState('');
  const {isSideBarActive, setIsSideBarActive} = useLayoutContext();
  
  const onSubmit = (e) => {
    e.preventDefault();
    e.stopPropagation();
    if (searchText) {
      router.push(`/search?query=${searchText}`);
      return;
    }
    router.push(`/`);
  }
  
  return (
    <Container>
      <MenuIcon onClick={() => setIsSideBarActive(!isSideBarActive)}>
        <FontAwesomeIcon icon={faFilm} />
      </MenuIcon>
      <Title>
        Movie App
      </Title>
      <SearchForm onSubmit={onSubmit}>
        <SearchInput
          placeholder="Search for a movie"
          value={searchText}
          onChange={e => setSearchText(e.target.value)}
        />
        <SearchButton type='submit'>
          Search
        </SearchButton>
      </SearchForm>
    </Container>
  )
}

export default TopBar