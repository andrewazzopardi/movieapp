import React from 'react'
import styled from 'styled-components';
import axios from 'axios';
import { useQuery } from 'react-query';

import SideBarMenuItem from './SideBarMenuItem';
import { genreListApi } from '../../../utils/api';

const Container = styled.div`
  padding: 12px 24px;
`;

function GenreSubMenu() {
  const {data: genresData} = useQuery('genres', () => axios.get(genreListApi));
  
  return (
    <Container>
      {genresData?.data.genres.map((item, index) => index < 6 && (
        <SideBarMenuItem key={item.id} label={item.name} url={`/genre/${item.id}`} />
      ))}
      <SideBarMenuItem label="More..." url="/genre" />
    </Container>
  )
}

export default GenreSubMenu