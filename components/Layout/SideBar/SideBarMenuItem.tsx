import { useRouter } from 'next/router';
import React from 'react';
import styled from 'styled-components';
import { useLayoutContext } from '../../../hooks/context';
import Link from '../../Link';

const Container = styled.div`
  cursor: pointer;

  &:hover {
    color: ${props => props.theme.sideBar.textHover};
  }
`;

const Title = styled.div`
  padding: 8px 12px;

  &:hover {
    background: ${props => props.theme.colors.primary}a0;
  }
`;


type SubMenuContainerProps = {
  isSubMenuOpen: boolean;
}

const SubMenuContainer = styled.div<SubMenuContainerProps>`
  height: 0;
  overflow: hidden;
  transition: height 0.2s ease-in-out;
  color: ${props => props.theme.sideBar.text};

  ${props => props.isSubMenuOpen && `
    height: 290px;
  `};
`;

type Props = {
  label: string;
  url: string;
  subMenuComponent?: React.ReactNode;
}

function SideBarMenuItem({ label, url, subMenuComponent }: Props) {
  const router = useRouter()
  const [isSubMenuOpen, setIsSubMenuOpen] = React.useState(false);
  const { setIsSideBarActive } = useLayoutContext();

  const onClickMenuItem = (nextUrl: string) => {
    setIsSideBarActive(false);
    router.push(nextUrl);
  }
  

  if (!subMenuComponent) {
    return (
      <Container onClick={() => onClickMenuItem(url)}>
        <Title>
          {label}
        </Title>
      </Container>
    )
  }

  return (
    <Container>
      <Title onClick={() => setIsSubMenuOpen(!isSubMenuOpen)}>
        {label}
      </Title>
      <SubMenuContainer isSubMenuOpen={isSubMenuOpen}>
        {subMenuComponent}
      </SubMenuContainer>
    </Container>
  )
}

export default SideBarMenuItem
