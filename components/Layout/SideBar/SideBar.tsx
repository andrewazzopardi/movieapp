import React from 'react'
import styled from 'styled-components';

import SideBarMenuItem from './SideBarMenuItem';
import GenreSubMenu from './GenreSubMenu';
import { useLayoutContext } from '../../../hooks/context';

type ContainerProps = {
  isActive: boolean;
}

const Container = styled.div<ContainerProps>`
  height: 100%;
  width: 200px;
  padding: calc(100px + 24px) 12px;
  background: ${props => props.theme.sideBar.background};
  color: ${props => props.theme.sideBar.text};
  position: absolute;
  top: 0;
  bottom: 0;
  left: ${props => props.isActive ? 0 : -200}px;
  transition: left 0.3s ease-in-out;
  z-index: 1;

  @media (min-width: 480px) {
    left: unset;
    bottom: unset;
    padding: calc(64px + 24px) 12px;
    z-index: 0
  }
`;

const sideBarMenuItems = [{
  label: 'Home',
  url: '/'
}, {
  label: 'Genres',
  url: '/genres',
  subMenuComponent: <GenreSubMenu />
}, {
  label: 'Statistics',
  url: '/statistics'
}];

function SideBar() {
  const {isSideBarActive} = useLayoutContext()
  
  return (
    <Container isActive={isSideBarActive}>
      {sideBarMenuItems.map(item => (
        <SideBarMenuItem key={item.url} label={item.label} url={item.url} subMenuComponent={item.subMenuComponent} />
      ))}
    </Container>
  )
}

export default SideBar