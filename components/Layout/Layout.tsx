import React, { useMemo, useState } from 'react'
import styled from 'styled-components';
import { LayoutContext } from '../../hooks/context';
import SideBar from './SideBar/SideBar';
import TopBar from './TopBar';

const Container = styled.div`
  min-height: 100vh;
  display: grid;
  grid-template-rows: auto 1fr;
  grid-template-columns: auto 1fr;
`;

const TopBarContainer = styled.div`
  grid-row: 1 / 2;
  grid-column: 1 / -1;
  z-index: 2;
  position: sticky;
  top: 0;
`;

const SideBarContainer = styled.div`
  grid-row: 1 / -1;
  grid-column: 1 / 2;
  position: sticky;
  top: 0;
  height: 100vh;
  z-index: 1;
  
  @media (min-width: 480px) {
    width: 200px;
  }
`;

const ContentContainer = styled.div`
  grid-row: 2 / -1;
  grid-column: 2 / -1;

  max-width: 100vw;
  max-height: calc(100vh - 101px);
  overflow: auto;

  @media (min-width: 480px) {
    max-width: calc(100vw - 200px);
    max-height: calc(100vh - 64px);
  }
`;

type Props = {
  children: React.ReactNode;
}

function Layout({ children }: Props) {
  const [isSideBarActive, setIsSideBarActive] = useState(false)
  
  const state = useMemo(() => ({
    isSideBarActive, setIsSideBarActive
  }), [isSideBarActive, setIsSideBarActive])

  return (
    <LayoutContext.Provider value={state}>
      <Container>
        <TopBarContainer>
          <TopBar />
        </TopBarContainer>
        <SideBarContainer>
          <SideBar />
        </SideBarContainer>
        <ContentContainer>
          {children}
        </ContentContainer>
      </Container>
    </LayoutContext.Provider>
  )
}

export default Layout