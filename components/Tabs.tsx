import React, { useState } from 'react'
import styled from 'styled-components';

const Container = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
`;

const TabList = styled.div`
  display: flex;
  background: #ffffff70;
  padding: 6px;
`;

type TabButtonProps = {
  isMobile?: boolean;
}

const TabButton = styled.div<TabButtonProps>`
  padding: 6px 12px;
  cursor: pointer;

  &:hover {
    background: #00000020;
  }

  ${props => props.isMobile && `
    @media (min-width: 960px) {
      display: none;
    }
  `}
`;

type TabContentProps = {
  activeTabIndex: number;
  tabCount: number;
}

const TabContent = styled.div<TabContentProps>`
  display: flex;
  overflow: hidden;

  & > div:first-child {
    margin-left: -${props => props.activeTabIndex * 100}%;
  }
`;

type TabWrapperProps = {
  isMobile?: boolean;
}

const TabWrapper = styled.div<TabWrapperProps>`
  width: 100%;
  flex-shrink: 0;
  transition: margin-left 0.2s ease-in-out;
  height: 100%;
  overflow: auto;
`;

export type TabProps = {
  label: string;
  isMobile?: boolean;
}

type Props = {
  children: React.ReactElement<TabProps>[];
  defaultActiveTab?: string;
  className?: string;
}

function Tabs({ children, defaultActiveTab, className }: Props) {
  const [activeTab, setActiveTab] = useState(defaultActiveTab || children[0].props.label);

  const renderedTabs = children.filter(child => child.props.label);
  const activeTabIndex = renderedTabs.findIndex(child => child.props.label === activeTab); 

  return (
    <Container className={className}>
      <TabList>
        {renderedTabs.map((child) => (
          <TabButton 
            key={child.props.label} 
            onClick={() => setActiveTab(child.props.label)}
            isMobile={child.props.isMobile}
          >
            {child.props.label}
          </TabButton>
        ))}
      </TabList>
      <TabContent activeTabIndex={activeTabIndex} tabCount={renderedTabs.length}>
        {renderedTabs.map((child) => (
          <TabWrapper key={child.props.label}>
            {child}
          </TabWrapper>
        ))}
      </TabContent>
    </Container>
  );
}

export default Tabs;