# MovieApp

I decided to change the following.

Instead of using D3.js directly I used a library called Recharts which is based on D3.js itself.
Recharts enables developers to create charts in React in a declarative way with Component-Like structure.

I decided to merge both charts instead of having to charts having the same values on the X-Axis.

# Demo

this website is available on: https://movieapp-eight-iota.vercel.app/
