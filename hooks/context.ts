import { createContext, useContext } from "react";

type AuthContextType = {
  user: any;
  updateUser: (user: any) => void;
};

const AuthContext = createContext<AuthContextType>({
  user: null,
  updateUser: () => 0,
});

const useAuthContext = () => useContext(AuthContext);

type LayoutContextType = {
  isSideBarActive: boolean;
  setIsSideBarActive: (nextState: boolean) => void;
}

const LayoutContext = createContext<LayoutContextType>({
  isSideBarActive: false,
  setIsSideBarActive: () => 0,
});

const useLayoutContext = () => useContext(LayoutContext);

export {
  LayoutContext,
  useLayoutContext,
  AuthContext,
  useAuthContext,
}