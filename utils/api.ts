export const movieApiBaseUrl = 'https://api.themoviedb.org/3';
export const movieApiKey = '643109d9b2ed389eb5b2c58a0819db73';
export const movieApiImageBaseUrl = 'https://image.tmdb.org/t/p/w500';

export const contructMovieApiUrl = (resource: string, params = '') => `${movieApiBaseUrl}/${resource}?${params}&api_key=${movieApiKey}&language=en-US`

export const movieListApi = contructMovieApiUrl('movie/popular', 'page=1');
export const genreListApi = contructMovieApiUrl('genre/movie/list');

export const getMovieDetailsApi = (movieId: string) => contructMovieApiUrl(`movie/${movieId}`);
export const getMovieCastApi = (movieId: string) => contructMovieApiUrl(`movie/${movieId}/credits`);
export const getMovieRecommendationApi = (movieId: string) => contructMovieApiUrl(`movie/${movieId}/recommendations`);

export const getMovieByGenreApi = (genreId: string, page = 1) => contructMovieApiUrl('discover/movie', `with_genres=${genreId}&page=${page}`);
export const getMovieBySearchApi = (search: string, page = 1) => contructMovieApiUrl('search/movie', `query=${search}&page=${page}`);
export const getMovieCreditsApi = (movieId: string) => contructMovieApiUrl(`movie/${movieId}/credits`);

export const getGuestSessionApi = () => contructMovieApiUrl(`authentication/guest_session/new`);
export const getGuestSessionRatedMoviesApi = (guestSessionId: string) => contructMovieApiUrl(`guest_session/${guestSessionId}/rated/movies`);

export const getRateMovieApi = (guestSessionId: string, movieId: string) => contructMovieApiUrl(`movie/${movieId}/rating`, `guest_session_id=${guestSessionId}`);