const colors = {
  primary: '#ff0000',
  secondary: '#000000',
  text: '#333333',
  textSecondary: '#ffffff',
  black: '#000000',
  red: '#ff0000',
  white: '#ffffff',
}

const textColors = {
  primary: colors.text,
  sideBar: colors.textSecondary,
}

const theme = {
  topBar: {
    background: colors.red+'d0',
    text: colors.white,
  },
  sideBar: {
    background: colors.black,
    text: colors.white,
    textHover: colors.red,
  },
  colors,
  textColors
}

export default theme;