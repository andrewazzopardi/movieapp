import axios from 'axios';
import { useRouter } from 'next/router';
import React from 'react'
import { useQuery } from 'react-query';
import CardContainer from '../components/CardContainer';
import MovieCard from '../components/MovieCard';
import { getMovieBySearchApi } from '../utils/api';

function SearchPage() {
  const router = useRouter();
  const query = router.query.query as string;

  const {data: moviesData}= useQuery(['movies', query], () => axios.get(getMovieBySearchApi(query)));

  return (
    <CardContainer>
      {moviesData?.data.results.map(movie => (
        <MovieCard key={movie.id} {...movie} />
      ))}
    </CardContainer>
  )
}

export default SearchPage;