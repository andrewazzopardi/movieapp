import React from "react";
import { AppProps } from "next/app";
import { createGlobalStyle, ThemeProvider } from "styled-components"
import Layout from "../components/Layout/Layout";
import theme from "../utils/theme";
import { QueryClient, QueryClientProvider } from "react-query";
import AuthProvider from "../components/Providers/Auth";
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const GlobalStyle = createGlobalStyle`
  body {
    padding: 0;
    margin: 0;
    font-family: 'Roboto', sans-serif;
  }

  * {
    box-sizing: border-box;
  }
`;

const queryClient = new QueryClient();

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
    
    <ToastContainer />
      <GlobalStyle />
      <ThemeProvider theme={theme}>
        <QueryClientProvider client={queryClient}>
          <AuthProvider>
            <Layout>
              <Component {...pageProps} />
            </Layout>
          </AuthProvider>
        </QueryClientProvider>
      </ThemeProvider>
    </>
  );
}
  
export default MyApp