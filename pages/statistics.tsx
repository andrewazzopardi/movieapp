//statistics page
import React from 'react';
import { useQuery } from 'react-query';
import axios from 'axios';
import styled from 'styled-components';
import { ResponsiveContainer, BarChart, CartesianGrid, XAxis, YAxis, Tooltip, Legend, Bar } from 'recharts';

const Container = styled.div`
  padding: 24px 24px 48px;
  height: 100%;
  width: 100%;
`;

function Statistics() {
  const { data } = useQuery('top_rated', () => axios.get('https://api.themoviedb.org/3/movie/top_rated?api_key=643109d9b2ed389eb5b2c58a0819db73'));  

  const top10Movies = data?.data.results.slice(0, 10) || [];

  return (
    <Container>
      <h1>Statistics</h1>
      <h2>Top Movie Ratings</h2>
      <div style={{ width: '100%', height: 500 }}>
        <ResponsiveContainer>
          <BarChart
            width={100}
            height={60}
            data={top10Movies}
          >
            <XAxis dataKey="title" tick={{}} />
            <YAxis yAxisId="left" domain={[0, 10]} tick={{color: 'red'}}/>
            <YAxis yAxisId="right" orientation="right" />
            <CartesianGrid strokeDasharray="3 3" />
            <Tooltip />
            <Legend verticalAlign='top' />
            <Bar name='Vote average' dataKey="vote_average"  yAxisId="left" fill="red" />
            <Bar name='Vote count' dataKey="vote_count" yAxisId="right" fill="black" />
          </BarChart>
        </ResponsiveContainer>
        </div>
    </Container>
  )
}  

export default Statistics;