import axios from 'axios'
import React from 'react'
import { useQuery } from 'react-query'
import CardContainer from '../components/CardContainer'
import MovieCard from '../components/MovieCard'

function index() {
  const { data } = useQuery('top_rated', () => axios.get('https://api.themoviedb.org/3/movie/top_rated?api_key=643109d9b2ed389eb5b2c58a0819db73'));

  return (
    <div>
      <CardContainer>
        {data?.data.results.map((movie: any) => (
          <MovieCard key={movie.id} {...movie} />
        ))}
      </CardContainer>
    </div>
  )
}

export default index