import React, { useState } from 'react'
import { useRouter } from 'next/router'
import { useQuery } from 'react-query';
import axios from 'axios';

import CardContainer from '../../components/CardContainer';
import { getMovieByGenreApi } from '../../utils/api';
import MovieCard from '../../components/MovieCard';

function MovieByGenrePage() {
  const router = useRouter();
  const genreId = router.query.genreId as string;
  const [page, setPage] = useState(1)

  const { data: moviesData } = useQuery(['moviesByGenre', genreId, page], () => axios.get(getMovieByGenreApi(genreId, page)));

  return (
    <CardContainer>
      {moviesData?.data.results.map((movie: any) => (
        <MovieCard key={movie.id} {...movie} />
      ))}
    </CardContainer>
  )
}

export default MovieByGenrePage