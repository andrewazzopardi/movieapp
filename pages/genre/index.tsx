import axios from 'axios'
import React from 'react'
import { useQuery } from 'react-query'
import Card from '../../components/Card'
import CardContainer from '../../components/CardContainer'
import Link from '../../components/Link'
import { genreListApi } from '../../utils/api'

function index() {
  const {data: genresData} = useQuery('genres', () => axios.get(genreListApi));

  return (
    <div>
      <CardContainer>
        {genresData?.data.genres.map((genre: any) => (
          <Link href={`/genre/${genre.id}`} key={genre.id}>
            <Card>
              {genre.name}
            </Card>
          </Link>
        ))}
      </CardContainer>
    </div>
  )
}

export default index