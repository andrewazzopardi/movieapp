import React from 'react'
import { useQuery } from 'react-query';
import axios from 'axios';
import { useRouter } from 'next/router';
import styled from 'styled-components';
import { getMovieCreditsApi, getMovieDetailsApi, movieApiImageBaseUrl } from '../../utils/api';
import Image from 'next/image';
import Tabs, { TabProps } from '../../components/Tabs';
import CardContainer from '../../components/CardContainer';
import PersonCard from '../../components/PersonCard';
import Rating from '../../components/Rating';

const Container = styled.div`
  height: 100%;
  display: flex;
  position: relative;
  overflow: hidden;
  
  @media (min-width: 960px) {
    max-width: calc(100vw - 200px);
  }
`;

const StyledTabs = styled(Tabs)`
  flex-basis: 550px;
  flex-grow: 1;
  min-width: 0;
`;

type BackgroundImageProps = {
  imageUrl?: string;
}

const BackgroundImage = styled.div<BackgroundImageProps>`
  position: absolute;
  top: -100px;
  left: -100px;
  right: -100px;
  bottom: -100px;
  background-image: url('${props => props.imageUrl}');
  background-position: left center;
  background-size: 200%;
  filter: blur(10px);
  z-index: -1;
`;


type PosterContainerProps = {
  isMobile?: boolean;
}

const PosterContainer = styled.div<PosterContainerProps>`
  position: relative;
  aspect-ratio: 2/3;
  
  ${props => props.isMobile ? `
    display: block;
    max-width: 300px;
    height: calc(100vh - 132px);
    margin: 0 auto;
  ` : `
    display: none;

    @media (min-width: 960px) {
      display: block;
    }
  `}
`;

const Tab = styled.div<TabProps>`
  padding: 12px;
`;

const InfoContainer = styled.div`
  background: #ffffff70;
  box-shadow: 2px 2px 2px 0 #0005;
  padding: 12px;
  font-size: 16px;
`;

const Title = styled.h1`
  margin: 0;
  font-size: 24px;
  margin-bottom: 8px;
`;

const tabs = ['Poster', 'Overview', 'Cast', 'Crew', 'Statistics'];

function MoviePage() {
  const router = useRouter();
  const movieId = router.query.movieId as string;

  const { data: movieData } = useQuery(['movie', movieId], () => axios.get(getMovieDetailsApi(movieId)));
  const { data: movieCreditsData } = useQuery(['movieCredits', movieId], () => axios.get(getMovieCreditsApi(movieId)));

  return (
    <Container>
      <BackgroundImage imageUrl={movieApiImageBaseUrl+movieData?.data.poster_path}/>
      <StyledTabs defaultActiveTab='Overview'>
        <Tab label="Poster" isMobile>
          <PosterContainer isMobile>
            <Image src={movieApiImageBaseUrl+movieData?.data.poster_path} layout="fill" objectFit='cover' />
          </PosterContainer>
        </Tab>
        <Tab label='Overview'>
          <InfoContainer>
            <div>{movieData?.data.release_date}</div>
            <Title>{movieData?.data.title}</Title>
            <div>Ratings: {Number(movieData?.data.vote_average).toFixed(1)}</div>
            <br />
            <div>Languages: {movieData?.data.spoken_languages.map(l => l.name).join(', ')}</div>
            <div>Genres: {movieData?.data.genres.map(genre => genre.name).join(', ')}</div>
            <br />
            <div>{movieData?.data.overview}</div>
          </InfoContainer>
          <Rating movieId={movieId} />
        </Tab>
        <Tab label='Cast'>
          <CardContainer>
            {movieCreditsData?.data.cast.map((crew: any) => (
              <PersonCard
                key={crew.id}
                title={crew.name}
                overview={crew.character} 
                poster_path={crew.profile_path}
              />
            ))}
          </CardContainer>
        </Tab>
        <Tab label='Crew'>
          <CardContainer>
            {movieCreditsData?.data.crew.map((crew: any) => (
              <PersonCard
                key={crew.id}
                title={crew.name}
                overview={crew.job}
                poster_path={crew.profile_path}
              />
            ))}
          </CardContainer>
        </Tab>
      </StyledTabs>
      <PosterContainer>
        <Image src={movieApiImageBaseUrl+movieData?.data.poster_path} layout="fill" objectFit='cover' />
      </PosterContainer>
    </Container>
  );
}

export default MoviePage;